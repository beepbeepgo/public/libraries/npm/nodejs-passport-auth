const baseConfig = require("@beepbeepgo/nodejs-testing-common/nyc.config.js");

module.exports = {
  ...baseConfig,
  lines: 70,
  statements: 70,
  functions: 70,
  branches: 50,
};
