const { chai } = require("@beepbeepgo/nodejs-testing-common");
const { expect } = chai;

const jwt = require("jsonwebtoken");
const createToken = require("../src/createToken");

describe("createToken", () => {
  const testUser = {
    id: 1,
    username: "testuser",
    role: "user",
  };
  const secretKey = "testSecretKey";

  it("should be a function", () => {
    expect(createToken).to.be.a("function");
  });

  it("should create a token with the correct payload", () => {
    const token = createToken(testUser, secretKey);
    const decoded = jwt.verify(token, secretKey);

    expect(decoded).to.have.property("id", testUser.id);
    expect(decoded).to.have.property("username", testUser.username);
    expect(decoded).to.have.property("role", testUser.role);
  });

  it("should create a token with a custom expiration time", () => {
    const expiresIn = "2h";
    const token = createToken(testUser, secretKey, expiresIn);
    const decoded = jwt.verify(token, secretKey);

    const now = Math.floor(Date.now() / 1000);
    const expectedExp = now + 2 * 60 * 60; // 2 hours in seconds

    expect(decoded).to.have.property("exp");
    expect(decoded.exp).to.be.closeTo(expectedExp, 2); // Allow for a 2-second margin
  });

  it("should create a token with a default expiration time of 1 hour", () => {
    const token = createToken(testUser, secretKey);
    const decoded = jwt.verify(token, secretKey);

    const now = Math.floor(Date.now() / 1000);
    const expectedExp = now + 1 * 60 * 60; // 1 hour in seconds

    expect(decoded).to.have.property("exp");
    expect(decoded.exp).to.be.closeTo(expectedExp, 2); // Allow for a 2-second margin
  });

  it("should throw an error when the secret key is incorrect", () => {
    const token = createToken(testUser, secretKey);

    expect(() => jwt.verify(token, "wrongSecretKey")).to.throw(
      jwt.JsonWebTokenError,
      "invalid signature"
    );
  });
});
