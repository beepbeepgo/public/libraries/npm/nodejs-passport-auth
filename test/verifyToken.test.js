const { chai, sinon } = require("@beepbeepgo/nodejs-testing-common");
const { expect } = chai;

const axios = require("axios");
const createToken = require("../src/createToken");
const verifyTokenByRole = require("../src/verifyToken");
const jwt = require("jsonwebtoken");

describe("verifyTokenByRole", () => {
  const testUser = {
    id: 1,
    username: "testuser",
    role: "user",
  };
  const secretKey = (process.env.SECRET_KEY = "testSecretKey");
  const userServiceURL = "http://example.com";
  const token = createToken(testUser, secretKey);

  let axiosGetStub;

  const mockUserModel = {
    findByPk: async (id) => {
      if (id === testUser.id) {
        return testUser;
      }
      return null;
    },
  };

  beforeEach(() => {
    axiosGetStub = sinon.stub(axios, "get");
    axiosGetStub
      .withArgs(`${userServiceURL}/api/users/verify`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .resolves({
        data: testUser,
      });
  });

  afterEach(() => {
    axiosGetStub.restore();
  });

  const testCases = [
    {
      title: "single role",
      requiredRoles: testUser.role,
    },
    {
      title: "multiple roles",
      requiredRoles: ["admin", testUser.role],
    },
  ];

  testCases.forEach((testCase) => {
    describe(testCase.title, () => {
      it("should grant access when the role matches the required role", async () => {
        const req = {
          headers: { authorization: `Bearer ${token}` },
        };
        const res = {
          status: () => ({
            send: () => {},
          }),
        };
        const next = sinon.spy();

        const middleware = verifyTokenByRole(
          testCase.requiredRoles,
          userServiceURL
        );
        await middleware(req, res, next);

        expect(next.calledOnce).to.be.true;
        expect(req.user).to.deep.equal(testUser);
      });

      it("should grant access when the role is included in the required roles", async () => {
        const req = {
          headers: { authorization: `Bearer ${token}` },
        };
        const res = {
          status: () => ({
            send: () => {},
          }),
        };
        const next = sinon.spy();

        const middleware = verifyTokenByRole(
          ["admin", testUser.role],
          userServiceURL
        );
        await middleware(req, res, next);

        expect(next.calledOnce).to.be.true;
        expect(req.user).to.deep.equal(testUser);
      });

      it("should deny access when the role does not match the required role", async () => {
        const req = {
          headers: { authorization: `Bearer ${token}` },
        };
        const res = {
          status: (code) => ({
            send: (response) => {
              expect(code).to.equal(403);
              expect(response).to.deep.equal({
                message: "You do not have permission to access this resource.",
              });
            },
          }),
        };
        const next = sinon.spy();

        const middleware = verifyTokenByRole(
          ["admin", "other"],
          userServiceURL
        );
        await middleware(req, res, next);

        expect(next.called).to.be.false;
      });
    });
  });

  it("should return an error when the authorization header is missing", async () => {
    const req = {
      headers: {},
    };
    const res = {
      status: (code) => ({
        json: (response) => {
          expect(code).to.equal(401);
          expect(response).to.deep.equal({
            message: "Authorization header is required",
          });
        },
      }),
    };
    const next = sinon.spy();

    const middleware = verifyTokenByRole(testUser.role, userServiceURL);
    await middleware(req, res, next);

    expect(next.called).to.be.false;
  });

  it("should return a 404 error when the user is not found", async () => {
    const nonExistingUserToken = jwt.sign(
      {
        id: 999,
        username: "nonexistent",
        role: "user",
      },
      secretKey
    );

    const req = {
      headers: { authorization: `Bearer ${nonExistingUserToken}` },
    };
    const res = {
      status: (code) => ({
        json: (response) => {
          //console.log(response);
          expect(code).to.equal(404);
          expect(response).to.deep.equal({
            message: "User not found",
          });
        },
      }),
    };
    const next = sinon.spy();

    const middleware = verifyTokenByRole(
      ["admin", "user"],
      null,
      mockUserModel
    );
    await middleware(req, res, next);

    expect(next.called).to.be.false;
  });

  it("should return a 401 error when the token is invalid", async () => {
    const invalidToken = "invalidToken";
    const req = {
      headers: { authorization: `Bearer ${invalidToken}` },
    };
    const res = {
      status: (code) => ({
        json: (response) => {
          expect(code).to.equal(401);
          expect(response).to.deep.equal({
            message: "Invalid or expired token",
          });
        },
      }),
    };
    const next = sinon.spy();

    const middleware = verifyTokenByRole(testUser.role, null, mockUserModel);
    await middleware(req, res, next);

    expect(next.called).to.be.false;
  });

  it("should return a 404 error when the user is not found", async () => {
    const nonExistentUser = {
      id: 999,
      username: "nonexistent",
      role: "user",
    };
    const nonExistentUserToken = createToken(nonExistentUser, secretKey);

    const req = {
      headers: { authorization: `Bearer ${nonExistentUserToken}` },
    };
    const res = {
      status: (code) => ({
        json: (response) => {
          expect(code).to.equal(404);
          expect(response).to.deep.equal({
            message: "User not found",
          });
        },
      }),
    };
    const next = sinon.spy();

    const middleware = verifyTokenByRole(testUser.role, null, mockUserModel);
    await middleware(req, res, next);

    expect(next.called).to.be.false;
  });
});
