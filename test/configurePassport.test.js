const { chai, sinon } = require("@beepbeepgo/nodejs-testing-common");
const { expect } = chai;

const configurePassport = require("../src/configurePassport");

// Dummy User model
const User = {
  findOne: async (query) => {},
  findByPk: async (id) => {},
};

describe("configurePassport", () => {
  let passport, findOneStub, findByPkStub, authenticateStub;

  beforeEach(() => {
    passport = configurePassport(User);

    findOneStub = sinon.stub(User, "findOne");
    findByPkStub = sinon.stub(User, "findByPk");

    // Return a valid user when the correct username is provided
    findOneStub.withArgs({ where: { username: "testuser" } }).resolves({
      id: 1,
      username: "testuser",
      password: "$2b$10$sVSGoBqCJ7n.koPFhTeyXeLltT1S8dSMLy4mDyf1E4NGs4PW4s5tO", // bcrypt hash for "password"
    });

    // Return a valid user when the correct user id is provided
    findByPkStub.withArgs(1).resolves({
      id: 1,
      username: "testuser",
      password: "$2b$10$sVSGoBqCJ7n.koPFhTeyXeLltT1S8dSMLy4mDyf1E4NGs4PW4s5tO", // bcrypt hash for "password"
    });

    // Mock the passport.authenticate method
    authenticateStub = sinon.stub(passport, "authenticate");

    // Return a function that calls the provided callback with the user object when the correct password is provided
    authenticateStub.withArgs("local").returns((req, res, next) => {
      const { username, password } = req.body;
      if (username === "testuser" && password === "password") {
        next(null, {
          id: 1,
          username: "testuser",
        });
      } else {
        next(null, false);
      }
    });
  });

  afterEach(() => {
    findOneStub.restore();
    findByPkStub.restore();
    authenticateStub.restore();
  });

  it("should be a function", () => {
    expect(configurePassport).to.be.a("function");
  });

  it("should return an object with use, serializeUser, and deserializeUser functions", () => {
    expect(passport).to.have.property("use").that.is.a("function");
    expect(passport).to.have.property("serializeUser").that.is.a("function");
    expect(passport).to.have.property("deserializeUser").that.is.a("function");
  });

  it("should authenticate a valid user", async () => {
    const request = {
      body: { username: "testuser", password: "password" },
    };
    const response = { end: () => {} };
    const authFunction = passport.authenticate("local");

    const user = await new Promise((resolve, reject) => {
      authFunction(request, response, (err, user) => {
        if (err) return reject(err);
        resolve(user);
      });
    });

    expect(user).to.be.an("object");
    expect(user).to.have.property("username", "testuser");
  });

  it("should not authenticate an invalid user", async () => {
    const request = {
      body: { username: "testuser", password: "wrongpassword" },
    };
    const response = { end: () => {} };
    const authFunction = passport.authenticate("local");

    const user = await new Promise((resolve, reject) => {
      authFunction(request, response, (err, user) => {
        if (err) return reject(err);
        resolve(user);
      });
    });

    expect(user).to.be.false;
  });

  it("should not authenticate when the user is not found", async () => {
    findOneStub.withArgs({ where: { username: "nonexistent" } }).resolves(null);

    const request = {
      body: { username: "nonexistent", password: "password" },
    };
    const response = { end: () => {} };
    const authFunction = passport.authenticate("local");

    const user = await new Promise((resolve, reject) => {
      authFunction(request, response, (err, user) => {
        if (err) return reject(err);
        resolve(user);
      });
    });

    expect(user).to.be.false;
  });

  it("should handle errors during the User.findOne call", async () => {
    findOneStub.rejects(new Error("findOne error"));

    const request = {
      body: { username: "testuser", password: "password" },
    };
    const response = { end: () => {} };
    const authFunction = passport.authenticate("local");

    try {
      await new Promise((resolve, reject) => {
        authFunction(request, response, (err, user) => {
          if (err) return reject(err);
          resolve(user);
        });
      });
    } catch (err) {
      expect(err).to.be.an("error");
      expect(err.message).to.equal("findOne error");
    }
  });

  it("should handle errors during the User.findByPk call", async () => {
    findByPkStub.rejects(new Error("findByPk error"));

    passport.deserializeUser("1", (err, user) => {
      expect(err).to.be.an("error");
      expect(err.message).to.equal("findByPk error");
    });
  });

  it("should not authenticate when the password does not match", async () => {
    const request = {
      body: { username: "testuser", password: "wrongpassword" },
    };
    const response = { end: () => {} };
    const authFunction = passport.authenticate("local");

    const user = await new Promise((resolve, reject) => {
      authFunction(request, response, (err, user) => {
        if (err) return reject(err);
        resolve(user);
      });
    });

    expect(user).to.be.false;
  });

  it("should serialize a user", (done) => {
    const user = {
      id: 1,
      username: "testuser",
      password: "$2b$10$sVSGoBqCJ7n.koPFhTeyXeLltT1S8dSMLy4mDyf1E4NGs4PW4s5tO", // bcrypt hash for "password"
    };

    passport.serializeUser(user, (err, id) => {
      expect(id).to.equal(user.id);
      done();
    });
  });

  it("should deserialize a user", (done) => {
    const user = {
      id: 1,
      username: "testuser",
      password: "$2b$10$sVSGoBqCJ7n.koPFhTeyXeLltT1S8dSMLy4mDyf1E4NGs4PW4s5tO", // bcrypt hash for "password"
    };

    passport.deserializeUser(user.id, (err, deserializedUser) => {
      expect(deserializedUser).to.deep.equal(user);
      done();
    });
  });

  it("should handle errors in the authentication process", async () => {
    findOneStub.throws(new Error("Unexpected error"));

    const request = {
      body: { username: "testuser", password: "password" },
    };
    const response = { end: () => {} };
    const authFunction = passport.authenticate("local");

    try {
      await new Promise((resolve, reject) => {
        authFunction(request, response, (err, user) => {
          if (err) return reject(err);
          resolve(user);
        });
      });
    } catch (error) {
      expect(error).to.be.an.instanceof(Error);
      expect(error.message).to.equal("Unexpected error");
    }
  });
});
