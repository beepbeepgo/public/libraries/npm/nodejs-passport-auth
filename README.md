# NodeJS Passport Auth

This a library that provides authentication and user management features using
Passport.js, a powerful and flexible authentication middleware for Node.js.
This library simplifies the process of implementing authentication in your
applications and microservices, whether you need to authenticate users with a
username and password or verify tokens for API access.

## Features

- Local authentication strategy with username and password
- JSON Web Token (JWT) creation and verification
- Easy integration with user management services and satellite microservices
- Pluggable and customizable, works with any data source that implements the
- required User model interface

## Installation

```bash
npm install @beepbeepgo/passport-auth
```

## Testing

```bash
git clone https://gitlab.com/beepbeepgo/public/libraries/npm/nodejs-passport-auth.git
cd node-passport-auth
npm ci
npm run test
```

## Usage

### User Management Service

To use `@beepbeepgo/passport-auth` in your user management service, you need
to configure Passport.js, create JWT tokens, and implement the required User
model interface.

1. Configure Passport.js:

```javascript
const configurePassport = require("@beepbeepgo/passport-auth/configurePassport");
const passport = configurePassport(User);
```

2. Create a JWT token:

```javascript
const createToken = require("@beepbeepgo/passport-auth/createToken");
const token = createToken(user, secretKey, expiresIn);
```

3. Implement the required User model interface:

Your User model should implement the following methods:

- `findOne(query)`: Finds a user by a query object (e.g., username)
- `findByPk(id)`: Finds a user by their primary key (e.g., user ID)

4. Set up auth middleware to protect routes.

```javascript
const verifyTokenByRole = require("@beepbeepgo/passport-auth/verifyToken");
const User = require("./models/User");

const app = express();

const requiredRole = "user";

app.use(verifyTokenByRole(requiredRole, null, User));
```

### Satellite Microservices

To use `@beepbeepgo/passport-auth` in a satellite microservice that needs to
verify a user's token, you need to configure the `verifyToken` middleware.

1. Configure the `verifyToken` middleware:

```javascript
const verifyTokenByRole = require("@beepbeepgo/passport-auth/verifyToken");
const { secretKey } = require("./config");

const app = express();

const userServiceURL = "http://users.example.com/";
const requiredRole = "user";

app.use(verifyTokenByRole(requiredRole, userServiceURL));
```

2. Access the authenticated user's data:

After the verifyToken middleware is configured, you can access the
authenticated user's data from the req.user object in your route handlers.

```javascript
app.get("/protected", (req, res) => {
  if (req.user) {
    res.json({ message: `Welcome, ${req.user.username}!` });
  } else {
    res.status(401).json({ message: "Unauthorized" });
  }
});
```

## Example

Here's a complete example of how to use @beepbeepgo/passport-auth in a user
management service:

```javascript
const express = require("express");
const { configurePassport, createToken } = require("@beepbeepgo/passport-auth");

const app = express();
app.use(express.json());

// Replace with your actual User model implementation
const User = {
  findOne: async (query) => {
    /_ ... _/;
  },
  findByPk: async (id) => {
    /_ ... _/;
  },
};

const passport = configurePassport(User);
const secretKey = "your-secret-key";
const expiresIn = "1h";

app.post("/login", passport.authenticate("local"), (req, res) => {
  const token = createToken(req.user, secretKey, expiresIn);
  res.json({ token });
});

// ... (other routes and configurations)

app.listen(3000, () => console.log("Server listening on port 3000"));
```

And in a satellite microservice:

```javascript
const express = require("express");
const verifyTokenByRole = require("@beepbeepgo/passport-auth/verifyToken");

const app = express();

const userServiceURL = "http://your-user-management-service-url";

// Middleware for user role
const verifyUser = verifyTokenByRole("user", userServiceURL);

// Middleware for admin role
const verifyAdmin = verifyTokenByRole("admin", userServiceURL);

// Unprotected route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to the public area." });
});

// Route protected by user role
app.get("/protected", verifyUser, (req, res) => {
  res.json({ message: `Welcome, ${req.user.username}!` });
});

// Route protected by admin role
app.get("/admin", verifyAdmin, (req, res) => {
  res.json({ message: `Welcome to the admin area, ${req.user.username}!` });
});

// ... (other routes and configurations)

app.listen(3000, () => console.log("Server listening on port 3000"));
```

In this example, we create two separate middlewares with different required
roles: `verifyUser` and `verifyAdmin`. We then apply these middlewares directly
to the routes we want to protect. The `/protected` route is protected by the
`verifyUser` middleware, while the `/admin` route is protected by the `verifyAdmin`
middleware. The root route `/` is not protected by any role-based middleware.

This way, you can easily set up different levels of access control for
various routes in your application.
