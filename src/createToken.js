const jwt = require("jsonwebtoken");

function createToken(user, secretKey, expiresIn = "1h") {
  const token = jwt.sign(
    {
      id: user.id,
      username: user.username,
      role: user.role,
    },
    secretKey,
    { expiresIn }
  );
  return token;
}

module.exports = createToken;
