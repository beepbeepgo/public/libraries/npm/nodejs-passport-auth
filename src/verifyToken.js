const axios = require("axios");
const jwt = require("jsonwebtoken"); // Add this for local token verification

const verifyTokenByRole = (requiredRoles, userServiceURL, User = null) => {
  if (!Array.isArray(requiredRoles)) {
    requiredRoles = [requiredRoles]; // Convert to an array if it's not an array already
  }

  return async (req, res, next) => {
    try {
      const authHeader = req.headers.authorization;

      if (authHeader) {
        const token = authHeader.split(" ")[1];
        let userData;

        if (User) {
          try {
            const decodedToken = jwt.verify(token, process.env.SECRET_KEY);
            userData = await User.findByPk(decodedToken.id);
            if (!userData) {
              return res.status(404).json({ message: "User not found" });
            }
          } catch (err) {
            return res
              .status(401)
              .json({ message: "Invalid or expired token" });
          }
        } else {
          // Verify token remotely if User model is not provided
          const response = await axios.get(
            `${userServiceURL}/api/users/verify`,
            {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            }
          );
          userData = response.data;
        }

        const { role } = userData;

        if (requiredRoles.includes(role)) {
          req.user = userData;
          next();
        } else {
          res.status(403).send({
            message: "You do not have permission to access this resource.",
          });
        }
      } else {
        res.status(401).json({ message: "Authorization header is required" });
      }
    } catch (error) {
      res
        .status(500)
        .json({ message: "An error occurred while processing the request" });
    }
  };
};

module.exports = verifyTokenByRole;
